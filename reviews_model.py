import re
from base_model import BaseModel

class ReviewsModel(BaseModel):
    MODEL_PATH = 'resources\\models\\reviews_model.h5'
    TOKENIZER_PATH = 'resources\\tokenizers\\reviews_tokenizer.pickle'
    MAX_WORDS_COUNT = 550

    def preprocess(self, text):
        text = text.lower()
        text = text.replace('ё', 'е')
        text = re.sub('[^a-zA-Zа-яА-Я1-9]+', ' ', text)
        text = re.sub(' +', ' ', text)
        return text.strip()

from keras.models import load_model
import pickle
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import metrics

class BaseModel():
    MODEL_PATH = ''
    TOKENIZER_PATH = ''
    MAX_WORDS_COUNT = 0

    def __init__(self):
        self.model = load_model(
            self.MODEL_PATH,
            custom_objects = {
                'precision': metrics.precision,
                'recall': metrics.recall,
                'f1': metrics.f1
            }
        )
        with open(self.TOKENIZER_PATH, 'rb') as handle:
            self.tokenizer = pickle.load(handle)

    def preprocess(self, text):
        return text.strip()

    def predict(self, text):
        cleared_text = self.preprocess(text)
        sequence = self.tokenizer.texts_to_sequences([cleared_text])
        sequence = pad_sequences(sequence, maxlen = self.MAX_WORDS_COUNT)
        return self.model.predict(sequence)[0][0]

from flask import Flask, request
from reviews_model import ReviewsModel
from tonality_analysis import TonalityAnalysis
from twitter_model import TwitterModel
from reviews_model import ReviewsModel
from subject_classifier import SubjectClassifier

app = Flask(__name__)

### Create tonality analysis module ###
tonality_analysis = TonalityAnalysis()

tonality_analysis.add_model('car_review', ReviewsModel())
tonality_analysis.add_model('rsm', TwitterModel())

subject_classifier = SubjectClassifier()
subject_classifier.add_class(0, 'car_review')
subject_classifier.add_class(1, 'rsm')

tonality_analysis.set_subject_classifier(subject_classifier)
#######################################

@app.route('/tonality_class_predict/', methods=['POST'])
def tonality_class_predict():
    if request.method == 'POST':
        assert request.form['text']
        return tonality_analysis.tonality_predict(
            request.form['text'],
            request.form.get('class'),
            request.form.get('threshold')
        )

if __name__ == '__main__':
    app.run()
class TonalityAnalysis():
    models = dict()

    def add_model(self, key, model):
        self.models[key] = model

    def set_subject_classifier(self, subject_classifier):
        self.subject_classifier = subject_classifier

    def get_class_by_value(self, value, threshold):
        if value > threshold:
            return 'positive'
        elif value < (1 - threshold):
            return 'negative'
        else:
            return 'mixed'

    def get_answer(self, text, predicted_class, sentiment, value):
        answer = dict()
        answer['text'] = text
        answer['class'] = predicted_class
        answer['sentiment'] = sentiment
        answer['positive'] = value
        answer['negative'] = 1. - value
        return answer

    def tonality_predict(self, text, predefined_class, threshold):
        if threshold is None:
            threshold = 0.7
        if predefined_class not in self.models:
            predefined_class = self.subject_classifier.predict(text)
        value = self.models[predefined_class].predict(text)
        sentiment = self.get_class_by_value(value, threshold)
        return self.get_answer(text, predefined_class, sentiment, value)

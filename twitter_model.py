import re
from base_model import BaseModel

class TwitterModel(BaseModel):
    MODEL_PATH = 'resources\\models\\twitter_model.h5'
    TOKENIZER_PATH = 'resources\\tokenizers\\twitter_tokenizer.pickle'
    MAX_WORDS_COUNT = 26

    def preprocess(self, text):
        text = text.lower()
        text = text.replace('ё', 'е')
        text = re.sub('((www\.[^\s]+)|(https?://[^\s]+))', 'URL', text)
        text = re.sub('@[^\s]+', 'USER', text)
        text = re.sub('[^a-zA-Zа-яА-Я1-9]+', ' ', text)
        text = re.sub(' +', ' ', text)
        return text.strip()

import pickle
import re

class SubjectClassifier():
    CLASSIFIER_PATH = 'resources/models/common_model.pickle'
    classes = dict()

    def __init__(self):
        with open(self.CLASSIFIER_PATH, 'rb') as handle:
            self.classifier = pickle.load(handle)

    def add_class(self, class_number, string_class_key):
        self.classes[class_number] = string_class_key

    def preprocess(self, text):
        text = text.lower()
        text = text.replace('ё', 'е')
        text = re.sub('((www\.[^\s]+)|(https?://[^\s]+))', 'URL', text)
        text = re.sub('@[^\s]+', 'USER', text)
        text = re.sub('[^a-zA-Zа-яА-Я1-9]+', ' ', text)
        text = re.sub(' +', ' ', text)
        return text.strip()

    def predict(self, text):
        cleared_text = self.preprocess(text)
        class_number = self.classifier.predict([cleared_text])[0]
        if class_number in self.classes:
            return self.classes[class_number] 
        else:
            return None